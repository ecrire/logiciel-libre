
----

### Les références

Utile quand insérer à la fin des autres fichiers pour faire des liens facilement.

[glossaire FACIL]: http://wiki.facil.qc.ca/view/Glossaire_de_l%27informatique_libre
	"Glossaire de l'informatique libre"

[fsf]: http://www.fsf.org/
	"Free Software Foundation"

