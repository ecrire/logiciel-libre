Glossaire
=========

Ce glossaire est conçu pour être lu dans l'ordre présenté, différent de l'ordre alphabétique habituel aux glossaires.

Voir aussi le [glossaire FACIL].

ordinateur
----------

logiciel / application
----------------------

système d'exploitation
----------------------

noyau (*kernel* / Linux)
------------------------

librairie (de fonctions)
------------------------

Internet
--------

World Wide Web
--------------
La Toile.

Serveur
-------

Client
------

Infonuagique (*cloud computing*)
--------------------------------

logiciel libre
--------------

free software
-------------

open source (software)
----------------------

logiciel à code source ouvert
-----------------------------

marché
------
Dans le sens de *free market*.

monopole
--------

droit d'auteur / copyright
--------------------------

attribution
-----------

plagiat
-------

brevet
------

marque de commerce
------------------

licence
-------

oeuvre dérivée
--------------

copyleft / share alike
----------------------
Dit Partage à l'identique.
