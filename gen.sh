# Script pour générer un document HTML incluant une table des matières
# à partir d'un fichier Markdown (et d'un fichier contenant des URL).
# Les références se trouvent dans references.md
# Le fichier généré se trouve dans le sous-répertoire tmp/
# Ce script dépend du logiciel pandoc pour fonctionner.

bn=$(basename $1 .md)
if [[ $bn == .md ]]
	then echo "spécifiez un fichier source comme arguement."
	else on=tmp/$bn.html
	cat $1 references.md |pandoc -o $on -f markdown -t html5 --smart --normalize --standalone --toc-depth=2 -
	echo "Généré: $on"
fi
