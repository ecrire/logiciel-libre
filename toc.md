Table des matières
==================

Préambule
---------
* La légende du plombier

Comment le logiciel libre s'insère-t-il dans divers environnement
-----------------------------------------------------------------
* Le logiciel libre pour soi
* Le logiciel libre sur soi (mobiles)
* Le logiciel libre à la maison (en appartement)
* Le logiciel libre en OBNL
* Le logiciel libre au bureau
* Le logiciel libre en classe
* Le logiciel libre à l'école
* Le logiciel libre à l'hopital
* Le logiciel libre au gouvernement
